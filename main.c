#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <time.h>
#include <stdlib.h>
#include <conio.h>

extern void abcstop ();
extern void abcplay (unsigned char channel, char *tune);
extern unsigned char abcactive[4];
// Special low-level calls to set up underlying hardware
// abcoctave legal values 0..6
extern void __fastcall__ abcoctave(unsigned char chan, unsigned char val);
// abcpitch legal values 0..255
extern void __fastcall__ abcpitch(unsigned char chan, unsigned char val);
// abctaps legal values 0..511
extern void __fastcall__ abctaps(unsigned char chan, unsigned int val);
// abcintegrate legal values 0..1
extern void __fastcall__ abcintegrate(unsigned char chan, unsigned char val);
// abcvolume legal values 0..127
extern void __fastcall__ abcvolume(unsigned char chan, unsigned char val);

static char palette[] = {
    0x9af >> 8,
    0x000 >> 8,
    0x333 >> 8,
    0x006 >> 8,
    0x01d >> 8,
    0x406 >> 8,
    0xd5f >> 8,
    0x5a5 >> 8,
    0xd34 >> 8,
    0x35f >> 8,
    0x929 >> 8,
    0x79f >> 8,
    0xb7e >> 8,
    0xfff >> 8,
    0x409 >> 8,
    0x81c >> 8,
    0x9af & 0xff,
    0x000 & 0xff,
    0x333 & 0xff,
    0x006 & 0xff,
    0x01d & 0xff,
    0x406 & 0xff,
    0xd5f & 0xff,
    0x5a5 & 0xff,
    0xd34 & 0xff,
    0x35f & 0xff,
    0x929 & 0xff,
    0x79f & 0xff,
    0xb7e & 0xff,
    0xfff & 0xff,
    0x409 & 0xff,
    0x81c & 0xff
};

static clock_t now;
static clock_t jumptime;
static clock_t preparejumptime;
static clock_t wavetime;
static clock_t fighttime;
static clock_t girlstep;
static clock_t frametime;
static clock_t shoottime;
static clock_t knockouttime;

extern unsigned char Landscape[];
extern unsigned char Santa0[];
extern unsigned char Sack[];
extern unsigned char Sack1[];
extern unsigned char Santa1[];
extern unsigned char Santa2[];
extern unsigned char Wave[];
extern unsigned char Scream[];
extern unsigned char Ball[];
extern unsigned char Splash[];
extern unsigned char Girl0[];
extern unsigned char Girl1[];
extern unsigned char GirlKnockout[];
extern unsigned char GirlDown[];
extern unsigned char Heart[];
extern unsigned char Snowflake[];
extern unsigned char Present[];
extern unsigned char SantaWins[];
extern unsigned char YouWin[];
extern unsigned char BeNice[];
extern unsigned char NotNaughty[];

static unsigned char parabel[] = {
 0,5,10,16,20,25,29,33,37,41,44,47,50,53,56,59,61,63,65,66,68,69,70,71,72
};

static unsigned char gparabel[] = {
 0,5,10,14,19,22,25,28,30,32,34,35,36
};

static signed char sparabel[] = {
 1,0,1,1,0,1,1,0,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0
};

static unsigned char parabelindex = 0;
static unsigned char parabeldirection = 0;
static unsigned char gparabelindex = 0;
static unsigned char gparabeldirection = 0;
static unsigned char knockout = 0;
static unsigned char gdown = 0;

static int SantaX = 80;
static int SantaY = 98;
static int GirlX = 20;
static int GirlY = 98;
unsigned int SantaHealth;

static unsigned char dojump = 0;
static unsigned char gdojump = 0;
static unsigned char frompos = 1;
static unsigned char topos = 2;
static unsigned char startwave = 0;
static unsigned char startfight = 0;
static unsigned char fightmode = 0;
static unsigned char playing;
static unsigned char gamewon = 0;
static unsigned char gamelost = 0;
static unsigned char gameaborted = 0;

typedef struct {
    unsigned char sprctl0;
    unsigned char sprctl1;
    unsigned char sprcoll;
    char *next;
    unsigned char *data;
    signed int hpos;
    signed int vpos;
    unsigned int hsize;
    unsigned int vsize;
    unsigned char h;
} sprite_t;

static SCB_REHV_PAL SScream = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | SKIP, NO_COLLIDE,
    0,
    Scream,
    80, 38,
    0x0100, 0x100,
    {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef}
};

static sprite_t SHeart2 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL,
    NO_COLLIDE,
    (char *)&SScream,
    Heart,
    20+50, 4,
    0x0100, 0x100,
    0
};

static sprite_t SHeart1 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL,
    NO_COLLIDE,
    (char *)&SHeart2,
    Heart,
    20+25, 4,
    0x0100, 0x100,
    0
};

static sprite_t SFlake3 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL,
    NO_COLLIDE,
    (char *)&SHeart1,
    Snowflake,
    40, 4,
    0x0100, 0x100,
    0
};

static sprite_t SFlake2 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL,
    NO_COLLIDE,
    (char *)&SFlake3,
    Snowflake,
    70, 37,
    0x0100, 0x100,
    0
};

static sprite_t SFlake1 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL,
    NO_COLLIDE,
    (char *)&SFlake2,
    Snowflake,
    120, 72,
    0x0100, 0x100,
    0
};

static sprite_t SBall8 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL,
    NO_COLLIDE,
    (char *)&SFlake1,
    Heart,
    20, 4,
    0x0100, 0x100,
    0
};

static sprite_t SBall7 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&SBall8,
    Ball,
    20, 40,
    0x0100, 0x100,
    0
};

static sprite_t SBall6 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&SBall7,
    Ball,
    40, 40,
    0x0100, 0x100,
    0
};

static sprite_t SBall5 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&SBall6,
    Ball,
    40, 40,
    0x0100, 0x100,
    0
};

static sprite_t SBall4 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&SBall5,
    Ball,
    40, 40,
    0x0100, 0x100,
    0
};

static sprite_t SBall3 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&SBall4,
    Ball,
    40, 40,
    0x0100, 0x100,
    0
};

static sprite_t SBall2 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&SBall3,
    Ball,
    40, 40,
    0x0100, 0x100,
    0
};

static sprite_t SBall1 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&SBall2,
    Ball,
    40, 40,
    0x0100, 0x100,
    0
};

static sprite_t SBall0 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&SBall1,
    Ball,
    60, 40,
    0x0100, 0x100,
    0
};

static SCB_REHV_PAL SGirl = {
    BPP_4 | TYPE_NORMAL | HFLIP,
    PACKED | REHV, NO_COLLIDE,
    (char *)&SBall0,
    Girl0,
    20, 98,
    0x0100, 0x100,
    {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef}
};

static SCB_REHV_PAL SSack = {
    BPP_4 | TYPE_BACKGROUND | HFLIP,
    PACKED | REHV, NO_COLLIDE,
    (char *)&SGirl,
    Sack1,
    80, 98,
    0x0100, 0x100,
    {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef}
};

static SCB_REHV_PAL SSanta = {
    BPP_4 | TYPE_BACKGROUND | HFLIP,
    PACKED | REHV, NO_COLLIDE,
    (char *)&SSack,
    Santa1,
    80, 98,
    0x0100, 0x100,
    {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef}
};

static SCB_REHV_PAL SHitSack = {
    BPP_4 | TYPE_NORMAL | HFLIP,
    PACKED | REHV | SKIP, NO_COLLIDE,
    (char *)&SSanta,
    Sack,
    60 + 20, 98 - 30,
    0x0100, 0x100,
    {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef}
};

static SCB_REHV_PAL SWaveRight = {
    BPP_4 | TYPE_BACKGROUND | HFLIP,
    PACKED | REHV | SKIP, NO_COLLIDE,
    (char *)&SHitSack,
    Wave,
    60, 102,
    0x0100, 0x100,
    {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef}
};

static SCB_REHV_PAL SWaveLeft = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV | SKIP, NO_COLLIDE,
    (char *)&SWaveRight,
    Wave,
    60, 102,
    0x0100, 0x100,
    {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef}
};

static SCB_REHV_PAL SLandscape = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV, NO_COLLIDE,
    (char *)&SWaveLeft,
    Landscape,
    0, 0,
    0x0100, 0x100,
    {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef}
};

static void newshot() {
    sprite_t *Spr = (sprite_t*)&SGirl;
    while (Spr->next != (char *)&SBall8) {
        Spr = (sprite_t*)(Spr->next);
        if ((Spr->sprctl1 & SKIP) != 0) {
            Spr->sprctl1 = PACKED | REHV | REUSEPAL;
            if (SantaX < GirlX) {
                Spr->sprctl0 = BPP_4 | TYPE_NORMAL;
            } else {
                Spr->sprctl0 = BPP_4 | TYPE_NORMAL | HFLIP;
            }
            Spr->hpos = GirlX;
            if (gdown == 1) {
                Spr->vpos = GirlY - (rand() & 3) - 1;
            } else {
                Spr->vpos = GirlY - 20 - (rand() & 7);
            }
            Spr->data = Ball;
            Spr->h = 0;
            return;
        }
    }
}

static int flightpath(sprite_t *Spr) {
    int ret = 3;

    if (Spr->h < sizeof(sparabel)) {
        ret = -sparabel[Spr->h];
    } else {
        if (Spr->h < 2 * sizeof(sparabel)) {
            ret = sparabel[2 * sizeof(sparabel) - Spr->h];
        }
    }
    Spr->h += 1;
    return ret;
}

static void reducehealth() {
    unsigned a = 1;
    if (a > SantaHealth) {
        SantaHealth = 0;
    } else {
        SantaHealth -= a;
    }
    if (SantaHealth < 1) {
        gamewon = 1;
        fightmode = 5;
        SSanta.sprctl1 |= SKIP;
        SSack.sprctl1 |= SKIP;
        SHitSack.sprctl1 |= SKIP;
        SScream.sprctl1 |= SKIP;
    }
}

static unsigned char Flakespeed = 1;

static void moveflakes() {
    if (Flakespeed == 0) {
    Flakespeed = 2;
    SFlake1.vpos += 1;
    SFlake2.vpos += 1;
    SFlake3.vpos += 1;
    if (SFlake1.vpos > 101+16) {
        SFlake1.vpos = -15;
    }
    if (SFlake2.vpos > 101+16) {
        SFlake2.vpos = -15;
    }
    if (SFlake3.vpos > 101+16) {
        SFlake3.vpos = -15;
    }
    }
    Flakespeed -= 1;
}

static void moveshots() {
    sprite_t *Spr = (sprite_t*)&SGirl;
    while (Spr->next != (char *)&SBall8) {
        Spr = (sprite_t*)(Spr->next);
        if ((Spr->sprctl1 & SKIP) == 0) {
            if ((Spr->data == Splash) && 
                (abs(Spr->hpos - SantaX) < 2)) {
                Spr->sprctl1 |= SKIP;
            }
            if (Spr->vpos > 97) {
                Spr->sprctl1 |= SKIP;
            }
            Spr->vpos += flightpath(Spr);
            if ((Spr->sprctl0 & HFLIP) == HFLIP) {
                Spr->hpos += 1;
                if ((SantaY == 98) && (abs(Spr->hpos - SantaX) < 8)
                    && (Spr->vpos > 97-58)) {
                    if (fightmode == 0) {
                        jumptime = preparejumptime = clock() + 300 - 50;
                        fightmode = 1;
                    }
                    Spr->data = Splash;
                    abcplay (3, "X200I0V20T4R0H10K1O0 g");
                    reducehealth();
                }
                if (Spr->hpos > 168) {
                    Spr->sprctl1 = PACKED | REHV | REUSEPAL | SKIP;
                }
            } else {
                Spr->hpos -= 1;
                if ((SantaY == 98) && (abs(Spr->hpos - SantaX) < 12)
                    && (Spr->vpos > 97-62)) {
                    if (fightmode == 0) {
                        jumptime = preparejumptime = clock() + 300 - 50;
                        fightmode = 1;
                    }
                    Spr->data = Splash;
                    abcplay (3,
                        "X200I0V20T4R0H10K1O0 g");
                    reducehealth();
                }
                if (Spr->hpos < -8) {
                    Spr->sprctl1 = PACKED | REHV | REUSEPAL | SKIP;
                }
            }
        }
    }
}

static void
init ()
{
  tgi_install (&tgi_static_stddrv);
  joy_install (&joy_static_stddrv);
  tgi_init ();
  CLI ();
  tgi_setpalette(palette);
  now = clock();
  wavetime = clock();
  fighttime = clock();
}

static void hitsanta()
{
    if (fightmode == 0) return;
    if (fightmode > 4) return;
    if (SantaY != 98) return;
    if (abs(GirlX - SantaX) > 30) return;
    if (knockout == 0) {
        abcplay (3, "X200I0V120T4R0H10K1O1 g");
        knockout = 1;
        knockouttime = clock() + 120;
        if ((SHeart2.sprctl1 & SKIP) == 0) {
            SHeart2.sprctl1 |= SKIP;
            return;
        }
        if ((SHeart1.sprctl1 & SKIP) == 0) {
            SHeart1.sprctl1 |= SKIP;
            return;
        }
        if ((SBall8.sprctl1 & SKIP) == 0) {
            SBall8.sprctl1 |= SKIP;
            return;
        }
        gamelost = 1;
        fightmode = 7;
        SGirl.sprctl1 |= SKIP;
        SFlake2.data = BeNice;
        SFlake3.data = SantaWins;
        SFlake1.data = NotNaughty;
    }
}

static void quake()
{
    if (startwave != 2) return;
    if (abs(GirlX - SantaX) > 80) return;
    if (gdojump == 1) return;
    if (knockout == 0) {
        abcplay (3, "X200I0V120T4R0H10K1O1 g");
        knockout = 1;
        knockouttime = clock() + 120;
        if ((SHeart2.sprctl1 & SKIP) == 0) {
            SHeart2.sprctl1 |= SKIP;
            return;
        }
        if ((SHeart1.sprctl1 & SKIP) == 0) {
            SHeart1.sprctl1 |= SKIP;
            return;
        }
        if ((SBall8.sprctl1 & SKIP) == 0) {
            SBall8.sprctl1 |= SKIP;
            return;
        }
        gamelost = 1;
        fightmode = 7;
        SGirl.sprctl1 |= SKIP;
        SFlake2.data = BeNice;
        SFlake3.data = SantaWins;
        SFlake1.data = NotNaughty;
    }
}

static void batting()
{
    if (gdown == 1) return;
    if (abs(GirlX - SantaX) > 80) return;
    if (fightmode != 4) return;
    if (startfight != 4) return;
    if (knockout == 0) {
        abcplay (3, "X200I0V120T4R0H10K1O1 g");
        knockout = 1;
        knockouttime = clock() + 120;
        if ((SHeart2.sprctl1 & SKIP) == 0) {
            SHeart2.sprctl1 |= SKIP;
            return;
        }
        if ((SHeart1.sprctl1 & SKIP) == 0) {
            SHeart1.sprctl1 |= SKIP;
            return;
        }
        if ((SBall8.sprctl1 & SKIP) == 0) {
            SBall8.sprctl1 |= SKIP;
            return;
        }
        gamelost = 1;
        fightmode = 7;
        SGirl.sprctl1 |= SKIP;
        SFlake2.data = BeNice;
        SFlake3.data = SantaWins;
        SFlake1.data = NotNaughty;
    }
}

static void winning()
{
    SFlake1.data = Present;
    SFlake2.data = YouWin;
    SFlake3.data = Present;
    if (joy_read(JOY_1) == 0) {
        fightmode += 1;
    }
}

static void facing()
{
    if (SantaX < GirlX) {
        SSanta.sprctl0 = BPP_4 | TYPE_BACKGROUND;
        SSack.sprctl0 = BPP_4 | TYPE_BACKGROUND;
        SGirl.sprctl0 = BPP_4 | TYPE_NORMAL | HFLIP;
    } else {
        SSanta.sprctl0 = BPP_4 | TYPE_BACKGROUND | HFLIP;
        SSack.sprctl0 = BPP_4 | TYPE_BACKGROUND | HFLIP;
        SGirl.sprctl0 = BPP_4 | TYPE_NORMAL;
    }
    hitsanta();
    quake();
    batting();
    if (knockout) {
        if ((gamelost == 0) && (clock() > knockouttime)) {
            knockout = 0;
            SGirl.data = Girl0;
            if (SantaX > 80) {
                GirlX = 10;
            } else {
                GirlX = 150;
            }
            SGirl.hpos = GirlX;
        } else {
            SGirl.data = GirlKnockout;
        }
    }
}

static void gjumping()
{
    if (gdojump) {
      if (gparabeldirection == 0) {
        if (gparabelindex < sizeof(gparabel) - 1) {
            gparabelindex += 1;
            GirlY = 98 - gparabel[gparabelindex];
            SGirl.vpos = GirlY;
        } else {
            gparabeldirection = 1;
        }
      } else {
        if (gparabelindex > 0) {
            gparabelindex -= 1;
            GirlY = 98 - gparabel[gparabelindex];
            SGirl.vpos = GirlY;
        } else {
            gparabeldirection = 0;
            gdojump = 0;
        }
      }
    }
}

static void walking()
{
  if (clock() > now) {
    if (SGirl.data == Girl0) {
        SGirl.data  = Girl1;
    } else {
        if (SGirl.data == Girl1) {
            SGirl.data = Girl0;
        }
    }
    now = clock() + 30;
  }
}

static void jumping()
{
    if (dojump) {
      if (parabeldirection == 0) {
        if (parabelindex < sizeof(parabel) - 1) {
            parabelindex += 1;
            SantaY = 98 - parabel[parabelindex];
            switch (frompos) {
            case 0:
                SantaX = 80 + 2*sizeof(parabel) - parabelindex;
                break;
            case 1:
                if (topos == 2) {
                    SantaX = 80 - parabelindex;
                } else {
                    SantaX = 80 + parabelindex;
                }
                break;
            case 2:
                SantaX = 80 - 2*sizeof(parabel) + parabelindex;
                break;
            }
            SSanta.vpos = SantaY;
            SSack.vpos = SantaY;
            SSanta.hpos = SantaX;
            SSack.hpos = SantaX;
        } else {
            parabeldirection = 1;
        }
      } else {
        if (parabelindex > 0) {
            parabelindex -= 1;
            SantaY = 98 - parabel[parabelindex];
            switch (frompos) {
            case 0:
                SantaX = 80 + parabelindex;
                break;
            case 1:
                if (topos == 2) {
                    SantaX = 80 - 2 * sizeof(parabel) + parabelindex;
                } else {
                    SantaX = 80  + 2 * sizeof(parabel) - parabelindex;
                }
                break;
            case 2:
                SantaX = 80 - parabelindex;
                break;
            }
            SSanta.vpos = SantaY;
            SSack.vpos = SantaY;
            SSanta.hpos = SantaX;
            SSack.hpos = SantaX;
        } else {
            parabeldirection = 0;
            if (topos > frompos) {
                frompos += 1;
                topos = frompos + 1;
                if (topos > 2) {
                    frompos = 2;
                    topos = 1;
                }
            } else {
                frompos -= 1;
                topos = frompos - 1;
                if (frompos < 1) {
                    frompos = 0;
                    topos = 1;
                }
            }
            SWaveLeft.hpos = SWaveRight.hpos = SantaX;
            dojump = 0;
            jumptime = preparejumptime = clock() + 300 - 50;
            wavetime = clock() + 5;
            startwave = 1;
        }
      }
    }
  if ((startwave > 0) && (clock() > wavetime)) {
     if (startwave == 1) {
         SWaveLeft.sprctl1 = SWaveRight.sprctl1 = PACKED | REHV;
         wavetime = clock() + 10;
         startwave = 2;
     } else {
         SWaveLeft.sprctl1 = SWaveRight.sprctl1 = PACKED | REHV | SKIP;
         startwave = 0;
     }
  }
  if (clock() > now) {
    if (SGirl.data == Girl0) {
        SGirl.data  = Girl1;
    } else {
        if (SGirl.data == Girl1) {
            SGirl.data = Girl0;
        }
    }
    if ((SSanta.data == Santa1) || (preparejumptime != jumptime)) {
      if (dojump == 0) {
          SSanta.data = Santa0;
          SSack.sprctl1 = PACKED | REHV | SKIP;
          if (clock() > preparejumptime) {
            if (fightmode < 4) {
                jumptime = clock() + 120;
            }
            preparejumptime = clock() + 300 - 50;
          }
          if (clock() > jumptime) {
            dojump = 1;
            fightmode += 1;
            SSanta.data = Santa2;
            jumptime = preparejumptime = clock() + 300 - 50;
          }
      }
    } else {
        if (jumptime == preparejumptime) {
          SSanta.data = Santa1;
          SSack.sprctl1 = PACKED | REHV;
        }
    }
    now = clock() + 30;
  }
}

static void fighting()
{
    SSanta.data = Santa1;
    switch (startfight) {
    case 0:
        SSack.sprctl1 = PACKED | REHV;
        fighttime = clock();
        startfight = 1;
        break;
    case 1:
        if (clock() > fighttime) {
            SScream.sprctl1 = PACKED | REHV;
            if (GirlX < SantaX) {
                SScream.hpos = SantaX - 10;
            } else {
                SScream.hpos = SantaX + 65;
            }
            fighttime = clock() + 60;
            startfight = 2;
        }
        break;
    case 2:
        if (clock() > fighttime) {
            SSack.sprctl1 = PACKED | REHV | SKIP;
            SScream.sprctl1 = PACKED | REHV | SKIP;
            SHitSack.sprctl1 = PACKED | REHV;
            if (GirlX < SantaX) {
                SScream.hpos = SantaX - 10;
                SHitSack.hpos = SantaX + 10;
                SHitSack.sprctl0 = BPP_4 | TYPE_NORMAL | HFLIP;
            } else {
                SScream.hpos = SantaX + 10;
                SHitSack.hpos = SantaX - 10;
                SHitSack.sprctl0 = BPP_4 | TYPE_NORMAL;
            }
            startfight = 3;
            fighttime = clock() + 30;
        }
        break;
    case 3:
        if (clock() > fighttime) {
            SSack.sprctl1 = PACKED | REHV | SKIP;
            SHitSack.sprctl1 = PACKED | REHV;
            if (GirlX < SantaX) {
                SHitSack.hpos = SantaX - 10;
                SHitSack.sprctl0 = BPP_4 | TYPE_NORMAL;
            } else {
                SHitSack.hpos = SantaX + 10;
                SHitSack.sprctl0 = BPP_4 | TYPE_NORMAL | HFLIP;
            }
            startfight = 4;
            fighttime = clock() + 30;
        }
        break;
    case 4:
        if (clock() > fighttime) {
            SSack.sprctl1 = PACKED | REHV;
            SHitSack.sprctl1 = PACKED | REHV | SKIP;
            startfight = 0;
            fightmode = 1;
            dojump = 0;
            jumptime = preparejumptime = clock() + 300 - 50;
        }
        break;
    }
}

static unsigned char partplaying = 0;

static void
loopmusic()
{
  if (abcactive[0]) return;
  switch (partplaying) {
  case 0:
    partplaying = 1;
    abcplay (0,
        "X7I0V80T10R0H10K3O1 Ge3d2c2G8  Ge3d2f2A8 Af3e2d2g8 ggzfzdze6z3 Ge3d2c2G8 Ge3d2f2A8 Af3e2d2ggzgzg2^g a2g2e2dc3zg5");
    abcplay (1,
        "X7I0V80T10R0H10K3O1 Ezz2E2z2z8 Ezz2A2z2E3F5 Fzz2G2z2B2c2B2c2 ^c2d2e2d2A2B2A2G2 Ezz2E2z2zAzzDz3 EzG2A2z2c3^d5 Fzz2G2z2cczczc2z ^f2e2_B2AG3z_e5");
    abcplay (2,
        "X7I0V60T10R0H10K3O1 D2z2^G2z2A,3^G,5 D4^F4C3_C5 C4C4G3G5 cczcz_Azc6B3 C2G2E2D2CDzC^EC3 C2D2E2D2Fcz_Ez_e3 D2D2E2E2_AAzAzA2z B2_B2^F2FD3zF5");
    break;
  case 1:
    partplaying = 2;
    abcplay (0,
        "X7I0V80T10R0H10K3O1 e'2e'2e'4e'e'ze'5e'g'3c'2d'2e'8zf'f'2f'2f'2f'e'ze'5e'2d'2d'e'3d'4g'4");
    abcplay (1,
        "X7I0V80T10R0H10K3O1 b2b2c'4bbzc'5gg3c2d2e'8 zaa2a2a2_agzg5|g2^f2f^g3a4z4");
    abcplay (2,
        "X7I0V60T10R0H10K3O1 C2B,2D2C2E2D2D2AG,C2B,2G2B,2C2D2^D2ECF2A2C2A2C2A2G2A2D2C2A2C2z2G2G,2G,4");
    break;
  case 2:
    partplaying = 3;
    abcplay (0,
        "X7I0V80T10R0H10K3O1 e'2e'2e'4e'e'ze'5e'g'3c'2d'2e'8zf'f'2f'2f'2f'e'ze'5e'g'2g'2f'd'2c'");
    abcplay (1,
        "X7I0V80T10R0H10K3O1 b2b2c'4bbzc'5gg3c2d2e'8 zaa2a2a2_agzg5 g2g2af2e");
    abcplay (2,
        "X7I0V60T10R0H10K3O1 C2B,2D2C2E2D2D2AG,C2B,2G2B,2C2D2^D2ECF2A2C2A2C2A2G2A2D2D,2C2G,2B,2");
    break;
  case 3:
    partplaying = 0;
    abcplay (0,
        "X7I0V80T10R0H10K3O1 c2zc'cc'c2c'2z2z4zd'^d'ze'c'gfef^fagecd_ecdcz4z4z_AcagecBGEDCDGEF_Aceg_bz_agg2zfe2zc'b2za^g2zf'e'2zd'c'2zc'b'2a'2g'a'b'_e'5z2zefefgec_AFECD^DEFe_edGBcz2z_B,5");
    abcplay (1,
        "X7I0V80T10R0H10K3O1 e2z2z4z2Dz3D2D3D5D2zD4zz3_E4zz2A2z2A2zD3zB2zz2_A2z2A2gzf_ee2zdzD3zD3D3D5zD3zD3 z2F2z2F2z2F2z2F2zzz2z_Az2zzzB5z2zF5");
    abcplay (2,
        "X7I0V60T10R0H10K3O1 C,2C2G,2^D,2C,2G,C,3^G,2G,3^G,5G,2C,^G,4 C,F3A,4A,D,2C2A,2C2A,E,3_B,D2_G,F,2D2D,2D2G,2F2_B,2F2zG,3z^G,3A,3^G,5zG,3z^G,3F,2_E2_E,2E2D2A2A,2A2zFz2zGz2GzzG5z2zG5");
    break;
  default:
    break;
  }

}

static void
gameloop ()
{
    unsigned char joy;

    SantaX = 80;
    SantaY = 98;
    GirlX = 20;
    GirlY = 98;
    SantaHealth = 7000;
    SGirl.sprctl1 = PACKED | REHV | REUSEPAL;
    SGirl.hpos = GirlX;
    SGirl.vpos = GirlY;
    SSanta.sprctl1 = PACKED | REHV | REUSEPAL;
    SSanta.hpos = SantaX;
    SSanta.vpos = SantaY;
    SSack.sprctl1 = PACKED | REHV | REUSEPAL;
    SSack.hpos = SantaX;
    SSack.vpos = SantaY;
    SHitSack.sprctl1 |= SKIP;
    SScream.sprctl1 |= SKIP;
    SBall8.sprctl1 = PACKED | REHV | REUSEPAL;
    SHeart2.sprctl1 = PACKED | REHV | REUSEPAL;
    SHeart1.sprctl1 = PACKED | REHV | REUSEPAL;
    dojump = 0;
    gdojump = 0;
    frompos = 1;
    topos = 2;
    startwave = 0;
    startfight = 0;
    fightmode = 0;
    jumptime = preparejumptime = clock() + 300 - 50;
    girlstep = clock();
    shoottime = clock();
    knockouttime = clock();
    while (playing == 1) {
        joy = joy_read(JOY_1);
        if (clock() > frametime) {
            moveflakes();
            moveshots();
            switch (fightmode) {
            case 0:
                walking();
                break;
            case 1:
            case 2:
            case 3:
                jumping();
                break;
            case 4:
                fighting();
                break;
            case 5:
                winning();
                break;
            case 6:
            case 7:
                if (JOY_BTN_FIRE(joy) || JOY_BTN_FIRE2(joy)) {
                    playing = 0;
                }
                break;
            }
            gjumping();
            frametime = clock();
        }
        if (JOY_BTN_RIGHT(joy)
            && (SGirl.data != GirlKnockout)) {
            if (clock() > girlstep) {
                GirlX += 4;
                if (GirlX > 150) GirlX = 150;
                SGirl.hpos = GirlX;
                girlstep = clock() + 1;
            }
        }
        if (JOY_BTN_LEFT(joy)
            && (SGirl.data != GirlKnockout)) {
            if (clock() > girlstep) {
                GirlX -= 4;
                if (GirlX < 10) GirlX = 10;
                SGirl.hpos = GirlX;
                girlstep = clock() + 1;
            }
        }
        if (JOY_BTN_DOWN(joy)
            && (SGirl.data != GirlKnockout)) {
            gdown = 1;
            SGirl.data = GirlDown;
        } else {
            if ((gdown == 1) &&
               (SGirl.data != GirlKnockout)) {
                SGirl.data = Girl0;
            }
            gdown = 0;
        }
        if (JOY_BTN_UP(joy)
            && (SGirl.data != GirlKnockout)) {
            if (gdojump == 0) {
                abcplay (3, "X7I0V100T8R0H2K1O1 g");
                gdojump = 1;
            }
        }
        if ((JOY_BTN_FIRE(joy) || JOY_BTN_FIRE2(joy))
            && (clock() > shoottime)
            && (SGirl.data != GirlKnockout)) {
            newshot();
            shoottime = clock() + 6 + (rand() & 7);
        }
        if (kbhit()) {
            gameaborted = 1;
            playing = 0;
        }
        if (!tgi_busy ()) {
            facing();
            tgi_sprite (&SLandscape);
            tgi_updatedisplay ();
        } else {
            loopmusic();
        }
    }
    while (joy_read(JOY_1) != 0)
        ;
    abcstop();
    abcvolume(0,0);
    abcvolume(1,0);
    abcvolume(2,0);
    abcvolume(3,0);
    abcplay (0,
	   "X7I1V127T10R6H10K1O2 e2|a4 e3d|^c6 A2|B^c de ^f^g ab|^g2 e3^e ^f^g| a2 e^c ^f2 dB|e2 ^cA ^GA Bc|de ^f^g ab ^c'd'|e'6 e2| d3^c B^c de|^c2 A4 e2|^f2 ed e^f ^ga|b6 e2| a2 e^c ^f2 dB|e2 ^cA ^GA Be|^f4 ^g3e|a6");
    while (abcactive[0] && (joy_read(JOY_1) == 0)) {
        while (tgi_busy())
            ;
        tgi_setcolor (1);
        tgi_bar(0, 0, 159, 101);
        tgi_setcolor (COLOR_GREEN + 1);
        tgi_outtextxy (4, 30, "Happy LynXmas 2020!");
        tgi_outtextxy (4, 50, "Be nice,");
        tgi_outtextxy (70, 50, "not naughty");
        tgi_outtextxy (50, 70, "-Karri");
        tgi_updatedisplay ();
    }
}

static void
letter ()
{
  unsigned char playing2 = 1;
  abcplay (0,
	   "X7 I0 V100 T10 R0 H8 K4 O1 z4 bgbg b2 g6 | z4 c'ac'a c'2 a6 | z4 bgbg a2 g6 z4 e'c'e'c' e'8");
  abcplay (1,
	   "X7 I0 V100 T10 R0 H8 K4 O2 z4 e'c'e'c' e'2 c'6 | z4 f'd'f'd' f'2 d'6 | z4 e'c'e'c' e'2 c'6 | z4 a'f'a'f' a'8");
  abcplay (2,
	   "X7 I0 V100 T10 R0 H8 K4 O2 cg7 fc'7 | cg7 | fc'7");

  while (abcactive[0] && playing2) {
    if (joy_read(JOY_1)) {
        playing2 = 0;
    }
    if (kbhit()) {
        playing2 = 0;
        gameaborted = 1;
    }
  while (tgi_busy ());
  tgi_setcolor (12);
  tgi_bar(0,0,159,101);
  tgi_setcolor (7);
  tgi_line(0,0,159,0);
  tgi_line(0,10,159,10);
  tgi_line(0,20,159,20);
  tgi_line(0,30,159,30);
  tgi_line(0,40,159,40);
  tgi_line(0,50,159,50);
  tgi_line(0,60,159,60);
  tgi_line(0,70,159,70);
  tgi_line(0,80,159,80);
  tgi_line(0,90,159,90);
  tgi_line(0,100,159,100);
  tgi_setcolor (4);
  tgi_outtextxy (64, 2, "LynXmas 2020");
  tgi_setcolor (2);
  tgi_outtextxy (0, 12, "dear santa");
  tgi_outtextxy (0, 32, "I'm writing to tell");
  tgi_outtextxy (0, 42, "you I've been");
  tgi_outtextxy (0, 52, "naughty and it was");
  tgi_outtextxy (0, 62, "worth it.");
  tgi_outtextxy (0, 82, "You fat, judgemental");
  tgi_outtextxy (0, 92, "bastard.");
  tgi_updatedisplay ();
  }
  while (abcactive[0] && joy_read(JOY_1)) ;
}

void
main (void)
{
    init ();
    while ((gamewon == 0) && (gameaborted == 0)) {
        gamewon = 0;
        gamelost = 0;
        gameaborted = 0;
        playing = 1;
        letter ();
        if (gameaborted == 0) {
            gameloop ();
        }
    }
    abcstop();
    while (tgi_busy())
        ;
    tgi_setpalette(tgi_getdefpalette());
    tgi_clear();
    tgi_updatedisplay();
    while (joy_read(JOY_1))
        ;
}
