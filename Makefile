include Common.mk

objects= \
	abcmusic.o \
	main.o \
	Landscape.o \
	Santa0.o \
	Sack.o \
	Sack1.o \
	Santa1.o \
	Santa2.o \
	Wave.o \
	Scream.o \
	Ball.o \
	Splash.o \
	Girl0.o \
	Girl1.o \
	GirlKnockout.o \
	GirlDown.o \
	Heart.o \
	Snowflake.o \
	Present.o \
	SantaWins.o \
	YouWin.o \
	BeNice.o \
	NotNaughty.o \

temporaryobjects= \
	Santa0.c \
	Sack.c \
	Sack1.c \
	Santa1.c \
	Santa2.c \
	Wave.c \
	Scream.c \
	Ball.c \
	Splash.c \
	Girl0.c \
	Girl1.c \
	GirlKnockout.c \
	GirlDown.c \
	Heart.c \
	Snowflake.c \
	Present.c \
	SantaWins.c \
	YouWin.c \
	BeNice.c \
	NotNaughty.c \

target = naughty.lnx naughty.o

all: $(target)

SantaWins.c : Naughty.pcx
	$(SP) -r $< --slice 301,0,41,25 -c lynx-sprite,mode=packed,ax=20,ay=13 -w $*.c,ident=$*

YouWin.c : Naughty.pcx
	$(SP) -r $< --slice 297,97,26,28 -c lynx-sprite,mode=packed,ax=13,ay=14 -w $*.c,ident=$*

BeNice.c : Naughty.pcx
	$(SP) -r $< --slice 307,29,31,25 -c lynx-sprite,mode=packed,ax=15,ay=13 -w $*.c,ident=$*

NotNaughty.c : Naughty.pcx
	$(SP) -r $< --slice 283,61,61,30 -c lynx-sprite,mode=packed,ax=30,ay=15 -w $*.c,ident=$*

Present.c : Naughty.pcx
	$(SP) -r $< --slice 227,26,30,26 -c lynx-sprite,mode=packed,ax=15,ay=13 -w $*.c,ident=$*

Snowflake.c : Naughty.pcx
	$(SP) -r $< --slice 280,0,16,16 -c lynx-sprite,mode=packed,ax=8,ay=8 -w $*.c,ident=$*

Heart.c : Naughty.pcx
	$(SP) -r $< --slice 258,27,24,29 -c lynx-sprite,mode=packed,ax=12,ay=0 -w $*.c,ident=$*

Girl0.c : Naughty.pcx
	$(SP) -r $< --slice 4,103,27,52 -c lynx-sprite,mode=packed,ax=15,ay=51 -w $*.c,ident=$*

Girl1.c : Naughty.pcx
	$(SP) -r $< --slice 41,102,28,52 -c lynx-sprite,mode=packed,ax=15,ay=51 -w $*.c,ident=$*

GirlKnockout.c : Naughty.pcx
	$(SP) -r $< --slice 200,124,54,30 -c lynx-sprite,mode=packed,ax=27,ay=29 -w $*.c,ident=$*

GirlDown.c : Naughty.pcx
	$(SP) -r $< --slice 75,110,26,43 -c lynx-sprite,mode=packed,ax=13,ay=42 -w $*.c,ident=$*

Santa0.c : Naughty.pcx
	$(SP) -r $< --slice 0,25,47,56 -c lynx-sprite,mode=shaped,ax=25,ay=55 -w $*.c,ident=$*

Sack.c : Naughty.pcx
	$(SP) -r $< --slice 159,56,65,25 -c lynx-sprite,mode=packed,ax=64,ay=24 -w $*.c,ident=$*

Scream.c : Naughty.pcx
	$(SP) -r $< --slice 159,0,65,51 -c lynx-sprite,mode=packed,ax=64,ay=24 -w $*.c,ident=$*

Ball.c : Naughty.pcx
	$(SP) -r $< --slice 234,5,7,7 -c lynx-sprite,mode=packed,ax=3,ay=3 -w $*.c,ident=$*

Splash.c : Naughty.pcx
	$(SP) -r $< --slice 260,5,17,17 -c lynx-sprite,mode=packed,ax=8,ay=8 -w $*.c,ident=$*

Sack1.c : Naughty.pcx
	$(SP) -r $< --slice 48,19,25,62 -c lynx-sprite,mode=shaped,ax=24,ay=61 -w $*.c,ident=$*

Santa1.c : Naughty.pcx
	$(SP) -r $< --slice 74,19,39,62 -c lynx-sprite,mode=shaped,ax=17,ay=61 -w $*.c,ident=$*

Santa2.c : Naughty.pcx
	$(SP) -r $< --slice 114,13,43,68 -c lynx-sprite,mode=shaped,ax=23,ay=67 -w $*.c,ident=$*

Wave.c : Naughty.pcx
	$(SP) -r $< --slice 0,0,100,10 -c lynx-sprite,mode=shaped,ax=99,ay=9 -w $*.c,ident=$*

naughty.lnx : $(objects)
	$(CL) -t lynx -o $@ -m naughty.map $(objects) $(others) lynx.lib

naughty.o : $(objects)
	$(CL) -t lynx --config lynx-bll.cfg -o $@ -m naughty.map $(objects) $(others) lynx.lib

clean:
	$(RM) $(objects) $(temporaryobjects) $(target) naughty.map

